// Ryan Appel

#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

string ranks[] = {
	"two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "jack", "queen", "king", "ace"
};

string suits[] = { "spades", "hearts", "clubs", "diamonds" };

enum Rank
{
	TWO = 2, THREE, FOUR, FIVE, SIX, SEVEN,
	EIGHT, NINE, TEN, JACK, QUEEN, KING, ACE
};

enum Suit { SPADE, HEART, CLUB, DIAMOND };

struct Card
{
	Rank rank;
	Suit suit;
};

void PrintCard(Card card)
{
	//cout << "The " << ranks[card.rank - TWO] << " of " << suits[card.suit];

	/**/
	cout << "The ";
	switch (card.rank)
	{
	case TWO: cout << "two"; break;
	case THREE: cout << "three"; break;
	case FOUR: cout << "four"; break;
	case FIVE: cout << "five"; break;
	case SIX: cout << "six"; break;
	case SEVEN: cout << "seven"; break;
	case EIGHT: cout << "eight"; break;
	case NINE: cout << "nine"; break;
	case TEN: cout << "ten"; break;
	case JACK: cout << "jack"; break;
	case QUEEN: cout << "queen"; break;
	case KING: cout << "king"; break;
	case ACE: cout << "ace"; break;
	}

	cout << " of ";
	switch (card.suit)
	{
	case HEART: cout << "hearts\n"; break;
	case CLUB: cout << "clubs\n"; break;
	case DIAMOND: cout << "diamonds\n"; break;
	case SPADE: cout << "spades\n"; break;
	}
	/**/
}

//todo: discuss with team
Card HighCard(Card c1, Card c2)
{
	if (c1.rank >= c2.rank) return c1;
	return c2;
}


int main()
{
	Card c;
	c.rank = NINE;
	c.suit = CLUB;

	Card c2;
	c2.rank = TEN;
	c2.suit = SPADE;

	PrintCard(HighCard(c, c2));

	_getch();
	return 0;
}

